"use strict"

module.exports = function(gulp, browserSync, options) {
  return function() {
    browserSync.init({
      port: options.port,
      server: {
        baseDir: options.paths.dist.root
      }
    })
  }
}
