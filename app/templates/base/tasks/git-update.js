"use strict"

module.exports = function(gulp, plugins, options) {

  function gitFetch() {
    gulp.start("git-merge")
  }

  gulp.task("git-update", function() {
    // see if current user has 'checkedout' the develop branch with no pending commits
    plugins.git.exec({args: "branch -l"}, function(err, stdout) {
      if (err) { throw err } else {
        // split out all available branches in an array
        var output = stdout.replace(/\s\s\s/g, ";").replace(/\n/g, "").split(";")
        // filter through branches to find the current branch indicator "*"
        for (var i = 0; i < output.length; i++) {
          if (output[i].indexOf("*") >= 0) {
            // remove the indicator from the current branch string and check if its the develop branch
            var curBranch = output[i].replace(/\*\s/g, "")
            if (curBranch === options.update.developmentBranch) {
              // progress to status check
              gulp.start("git-status")
            } else {
              // user isn't using the development branch present them with error
              console.log("ERROR: You need to checkout the develop branch to continue.")
            }
          }
        }
      }
    })
  })

  gulp.task("git-status", function() {
    // check to see if the development branch is ahead or behind the remote branch
    plugins.git.status({args: "--porcelain"}, function(err, stdout) {
      if (err) { throw err } else {
        if (stdout.length === 0) {
          // progress to tag merging
          gulp.start("git-branch-log")
        } else {
          // the users local development branch is ahead or behind the remote branch, throw error
          console.log("ERROR: The development branch is ahead or behind the remote branch. Please pull/commit/push as necessary to continue.")
        }
      }
    })
  })

  gulp.task("git-branch-log", function() {
    // retrieve a list of local git branches
    plugins.git.exec({args: "branch -l"}, function(err, stdout) {
      if (err) { throw err } else {
        // split out all available branches in an array
        var output     = stdout.replace(/\s\s\s/g, ";").replace(/\*\s|\n/g, "").split(";")
        // search array for tag update branch
        var condBranch = output.indexOf(options.update.tagBranch)
        // check to see if branch already exists
        if (condBranch >= 0) {
          // if it does checkout the tag update branch
          gulp.start("git-checkout-tag")
        } else {
          // if it doesn't create the branch
          gulp.start("git-branch")
        }
      }
    })
  })

  gulp.task("git-branch", function() {
    // create new tag branch
    plugins.git.branch(options.update.tagBranch, function(err) {
      if (err) { throw err } else {
        // if created successfully check it out
        gulp.start("git-checkout-tag")
      }
    })
  })

  gulp.task("git-checkout-tag", function(done) {
    // checkout the tag merge branch if already existing or just created
    plugins.git.checkout(options.update.tagBranch, function(err) {
      if (err) { throw err } else {
        // if the checkout was successful display the current branch
        gulp.start("git-start-update")
      }
    })
  })

  gulp.task("git-checkout-develop", function(done) {
    // checkout the develop branch
    plugins.git.checkout(options.update.developmentBranch, function(err) {
      if (err) { throw err } else {
        // if the checkout was successful merge in the tag merge branch
        console.log("Update successful or no update required. Be sure to double check the development branch to see if merged tag changes need to be committed. Have a nice day! :)")
        done()
      }
    })
  })

  gulp.task("git-start-update", ["git-upstream"], function() {
    console.log("Checking for updates...")
    gulp.start("git-fetch")
  })

  // Checks for upstream repo or sets it if doesn't exist
  gulp.task("git-upstream", function(done) {
    // Check the upstreams exists
    plugins.git.exec({args: "remote -v"}, function(err, stdout) {
      if (err) { throw err } else {
        // No errors; Get the upstream value
        var output      = stdout.replace(/\s/g, " ").split(" "),
            upstreamSrc = output[output.indexOf("upstream") + 1]

        if (upstreamSrc !== options.update.source) {
           // add the upstream origin
          plugins.git.addRemote("upstream", options.update.source, function(err) {
            if (err) { throw err } else {
              // No errors
              console.log("Added the new upstream origin...")
              done()
            }
          })
        } else {
          console.log("Correct upstream already set...")
          done()
        }
      }
    })
  })

  // Fetch the latest from the upstream repo
  gulp.task("git-fetch", function() {
    console.log("Fetching upstream...")
    // Fetch command
    plugins.git.fetch("upstream", "", function(err) {
      if (err) { throw err } else {
        // Do the merge
        gulp.start("git-merge")
      }
    })
  })

  // Merge the latest tag into this repo
  gulp.task("git-merge", function() {
    console.log("Merging...")
    // Get the latest tag
    plugins.git.tag( function (result) {
      // No errors; Get the alatest tag
      var latestTag = result.slice(-2)[0]; // result array returns an empty value as the last index, so we need the -2 to get the most recent
      // Merging
      plugins.git.merge(latestTag, function(err) {
        if (err) {
          console.log("### You either need to commit your files before running the `$ gulp git-update` command OR there are conflicts in which case consult a doctor. ###")
          throw err
        } else {
          // No errors go back to develop
          options.deployment.environment = "tag-merge"
          gulp.start("deploy")
        }
      })
    })
  })
}
