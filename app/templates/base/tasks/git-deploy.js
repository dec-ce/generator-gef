"use strict"

module.exports = function(gulp, plugins, options, runSequence) {

  // Pull first to catch any updates
  gulp.task("git-pull", function(cb) {
    return plugins.git.pull('origin', 'master', {cwd: options.deployment.repository.root}, function (err) {
      if (err) {
        throw err
      } else {
        runSequence('git-deploy', cb)
      }
    });
  });

  // Copy the files
  gulp.task("git-cp", function() {
    return gulp.src(options.deployment.source)
    .pipe(gulp.dest(options.deployment.repository.deploymentLocation))
  })

  // Add any new files to the repo
  gulp.task("git-add", ["git-cp"], function() {
    return gulp.src("./.", {cwd: options.deployment.repository.root})
    .pipe(plugins.git.add({cwd: options.deployment.repository.root}, function(err) {
      if (err) {
        throw err
      } else {
        console.log('Added new files!');
      }
    }))
  });

  // Automated commit message
  gulp.task("git-commit", ["git-add"], function() {
    return gulp.src("./", {cwd: options.deployment.repository.root})
    .pipe(plugins.git.commit("Automated deployment to GEF " + options.deployment.environment + " repository", {cwd: options.deployment.repository.root}, function(err) {
      this.emit("end")
      console.log("The " + options.deployment.environment + " environment is already up to date. Nothing to be added :D")
    })
    )
  })

  // Push that code.
  gulp.task("git-deploy", ["git-commit"], function(cb) {
    return plugins.git.push("origin", "master", options.deployment.args, function(err) {
      if (err) {
        throw err
      } else {
        console.log("You've successfully updated the " + options.siteName + " " + options.deployment.environment + " environment! Give yourself a pat on the back :) ")
        gulp.src(__filename)
          .pipe(plugins.open({uri: "http://dec-ce.bitbucket.com/" + options.siteName.replace(/\s+/g, "-").toLowerCase() + "/" + options.deployment.environment + "/"}))
      }
    })
  })
}
