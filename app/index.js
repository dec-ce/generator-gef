"use strict"

var yo    = require('yeoman-generator'),
    chalk = require('chalk')


// Capitalize First letter in strings
String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase()})
}


module.exports = yo.Base.extend({


  prompting: function() {

    console.log(chalk.green('====================================================================================='))
    console.log(chalk.green('       _____   _____   __    ,  _____   ____     ___    ______,  ______   ____       '))
    console.log(chalk.green('      /       /       /  |  /  /       /    |   /   |     /     /     /  /    |      '))
    console.log(chalk.green('     /   __  /____   /   | /  /___    /____/   /____|    /     /     /  /____/       '))
    console.log(chalk.green('    /____/  /_____  /    |/  /_____  /    |_  /     |   /     /____ /  /    |_       '))
    console.log(chalk.green(''))
    console.log(chalk.green('|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||'))
    console.log(chalk.green('|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||'))
    console.log(chalk.green('||                       | ||||                      | ||||                      | ||'))
    console.log(chalk.green('||     __________________| ||||      ________________| ||||       _______________| ||'))
    console.log(chalk.green('||     |  _________________||||      |  _______________||||      |  _______________||'))
    console.log(chalk.green('||     | ||||||||||||||||||||||      | ||||||||||||||||||||      | ||||||||||||||||||'))
    console.log(chalk.green('||     | ||||||||||||||||||||||                 | |||||||||                | ||||||||'))
    console.log(chalk.green('||     | ||||||          | ||||      ___________| |||||||||      __________| ||||||||'))
    console.log(chalk.green('||     | ||||||_____     | ||||      |  __________|||||||||      |  _________||||||||'))
    console.log(chalk.green('||     | |||||||____|    | ||||      | ||||||||||||||||||||      | ||||||||||||||||||'))
    console.log(chalk.green('||     | ||||||||||||    | ||||      | ||||||||||||||||||||      | ||||||||||||||||||'))
    console.log(chalk.green('||                       | ||||                      | ||||      | ||||||||||||||||||'))
    console.log(chalk.green('||_______________________| ||||______________________| ||||______| |||||||||| NSW |||'))
    console.log(chalk.green('|||________________________|||||_______________________|||||_______|||||||||| DoE |||'))
    console.log(chalk.green('|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||'))
    console.log(chalk.green('====================================================================================='))

    console.log(chalk.bold.dim('============================'))
    console.log(chalk.bold.bgBlue('Initialising...'))
    console.log(chalk.bold.dim('============================'))

    var done  = this.async()

    // Get information from the user
    this.prompt([
      {

        // Get the project name
        type    : 'input',
        name    : 'name',
        message : 'What is the project name?',
        default : this.appname
      }, {

        // Description of the package
        type    : 'input',
        name    : 'description',
        message : 'How would describe this site?'
      }, {

        // Get the repo address
        type    : 'input',
        name    : 'repo',
        message : 'What is its git URL?',
        default : 'https://bitbucket.org/dec-ce/' + this.appname.replace(/\s+/g, '-')
      }, {

        // Get git username
        type    : 'input',
        name    : 'user',
        message : 'What is your git username?'
      }
    ], function (answers) {

      // Save the results for later
      answers.origin    = 'https://' + answers.user + '@' + answers.repo.replace(/(https:\/\/|http:\/)/, '')
      answers.appname   = this.appname.replace(/\s+/g, '-')
      this.answers      = answers


      // Show the answers
      this.log(chalk.bold.dim('========================================================='))
      this.log(chalk.bold.bgBlue('Project Name: ') + chalk.bold.blue(answers.name))
      this.log(chalk.bold.bgBlue('Git origin: ') + chalk.bold.blue(this.answers.origin))
      this.log(chalk.bold.dim('========================================================='))

      done()
    }.bind(this))

  },

  // Configurations
  configuring: function() {
    //console.log('Setting configurations...')
  },

  // Writing Files
  writing: function() {
    // Get configuration
    var answers   = this.answers,
        sitename  = answers.name.capitalize()

    console.log(chalk.bold.bgBlue('Building ' + sitename + ' structure...'))
    console.log(chalk.bold.dim('============================'))

    // Copy required bulk files
    this.directory(
      this.templatePath('base/'),
      this.destinationPath()
    )

    // Copy package.json and modify object
    this.fs.copyTpl(
      this.templatePath('package.json'),
      this.destinationPath('package.json'),
      {
        appname : answers.appname,
        desc    : answers.description
      }
    )

    // Copy bower.json and modify object
    this.fs.copyTpl(
      this.templatePath('bower.json'),
      this.destinationPath('bower.json'),
       {
        appname : answers.appname,
        desc    : answers.description
      }
    )

    // Copy the default README.md
    this.fs.copyTpl(
      this.templatePath('README.md'),
      this.destinationPath('README.md'),
       {
        sitename: sitename,
        desc    : answers.description
      }
    )

    // Copy gulpfile set the sitename
    this.fs.copyTpl(
      this.templatePath('gulpfile.js'),
      this.destinationPath('gulpfile.js'),
      { sitename: sitename }
    )

  },

  // Installations
  install: function() {

    var sc    = this.spawnCommand,
        _yo   = this,
        done  = this.async()

    // Initialise git repo
    function gitInit() {
      console.log(chalk.bold.bgBlue('Initalizing Git repo...'))
      sc('git', ['init']).on('close', gitUpstream)
    }

    // Set the upstream origin
    function gitUpstream() {
      console.log(chalk.bold.bgBlue('Setting the remote origin...'))
      sc('git', ['remote', 'add', 'origin', _yo.answers.origin]).on('close', gitAdd)
    }

    // Add files to start tracking
    function gitAdd() {
      console.log(chalk.bold.bgBlue('Adding files to Git...'))
      sc('git', ['add', '-A']).on('close', gitCommit)
    }

    // Make the first commit
    function gitCommit() {
      console.log(chalk.bold.bgBlue('Doing a first commit...'))
      console.log(chalk.bold.dim('============================'))
      sc('git', ['commit', '-a', '-m', 'First\ Commit']).on('close', installBower)
    }

    // Install Deps
    // this.installDependecies() wasn't working so doing manual work around
    function installBower() {
      console.log(chalk.bold.bgBlue('Installing components...'))
      console.log(chalk.bold.dim('============================'))
      sc('bower', ['install']).on('close', installNpm)
    }
    function installNpm() {
      console.log(chalk.bold.dim('============================'))
      console.log(chalk.bold.bgBlue('Installing packages...'))
      console.log(chalk.bold.dim('============================'))
      sc('npm', ['install']).on('close', setUpstream)
    }

    // Set the Upstream repo for future updates
    function setUpstream() {
      console.log(chalk.bold.dim('============================'))
      console.log(chalk.bold.bgBlue('Setting the upstream repo'))
      console.log(chalk.bold.dim('============================'))
      sc('gulp', ['git-upstream']).on('close', buildDist)
    }

    // Do initial build
    function buildDist() {
      console.log(chalk.bold.dim('============================'))
      console.log(chalk.bold.bgBlue('Doing an initial build...'))
      console.log(chalk.bold.dim('============================'))
      sc('gulp', ['build']).on('close', done)
    }

    gitInit()
  },

  // End
  end: function() {
    var sc    = this.spawnCommand,
        done  = this.async()
    console.log(chalk.bold.bgGreen('Finished Installation...'))
    console.log(chalk.bold.dim('============================'))
    console.log(chalk.bold.bgBlue('You may want to run `$ gulp git-update` followed by `$ bower install` to get the latest code from GEF.'))

    // Ask the user if they want to launch the server
    this.prompt([
      {
        // Description of the package
        type    : 'confirm',
        name    : 'launch',
        message : 'Do you want to launch the server?',
        default : false
      }
    ], function(answers) {
      // launch the server if true
      if (answers.launch == true) {
        sc('gulp')
      } else {
         console.log(chalk.bold.bgGreen('All done... Have a nice day! :)'))
         done()
      }
    })
  }
})