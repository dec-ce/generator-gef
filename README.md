# Generator GEF
> A Yeoman Generator to spin up GEF projects quickly

###note: this repository is very out of date and would need a fair bit of dusting and bringing up to speed before being usable.

Author: NSW Department of Education

Maintainers: [Alex Dewez-Lopez](https://github.com/dewezlopez)

## Setting up

By now you should have node.js and npm installed. If not;

- OSX: Use one of these methods: https://gist.github.com/isaacs/579814
- WIN: https://nodejs.org/en/download/stable/ (needs to be tested)

### Once you have node.js & npm installed

1. `$ npm install -g yo && npm install -g bower && npm install -g gulp` - Install Yeoman, Bower and Gulp
2. `$ npm install -g https://bitbucket.org/dec-ce/generator-gef` - Install the Generator *Note: Depending on your setup, you may be prompted for your user name and password, this is normal.*


## Using the Generator

1. a. MACOS `$ mkdir your/path/to/project-name && cd your/path/to/project-name` - Create and go to a directory for your new GEF project
1. b. WIN `$ mkdir your\path\to\project-name && cd your/path/to/project-name`
2. `$ yo gef` - Run the generator!!

## Updating the code base

1. `$ gulp update-gef` - fetch the latest from [GEF Generator Master](https://bitbucket.org/dec-ce/gef-generator-master) (not to be confused with Generator GEF)
2. `$ git mergetool --tool=opendiff` - Fix the conflicts, you should be merging everything except your project related lines (names, descriptions, etc)

## Troubleshooting

You may come across errors during the npm package install if you haven't set up your node permissions properly - this would be the case if you are always using `$ sudo npm install` in order to force things to work.

*NSW Department of Education recommends you spend the time fixing your node installation if this is the case. [Here is a starting point.](https://docs.npmjs.com/getting-started/fixing-npm-permissions)*

## Under the hood

Once you've answered the initial questions, the Generator does the following:

- Sets up your development environment
- Initialises your local Git repo and makes your first commit
- Adds the upstream repository so you can update to the latest code base
- Installs your components and packages
- Runs an initial build so your dist/ directory is ready to go
- Gives you the option to start the server once everything is completed